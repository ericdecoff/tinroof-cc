# TinRoof-CC
## NodeJS required
[ https://nodejs.org/en/ ] ( https://nodejs.org/en/)

## NPM Install Packages
```
npm install
```


## Local Postgres required
[ https://www.bigsql.org/postgresql/installers.jsp ](https://www.bigsql.org/postgresql/installers.jsp)

## Create Database
```
createdb -U postgres todos
```

## Create Role
```
psql -U postgres -c "CREATE ROLE admin LOGIN SUPERUSER INHERIT CREATEDB CREATEROLE" todos
```

## Grant Permissions
```
psql -U postgres -c "GRANT ALL ON items TO admin" todos
```

## Create Table Items
```
node create 
```

## Validate Table
```
psql -U admin todos

# \d Items

            List of relations
 Schema |     Name     |   Type   | Owner
--------+--------------+----------+-------
 public | items        | table    | admin
 public | items_id_seq | sequence | admin

# \q
```

## Start Server
```
node start 
```

## App
[ http://localhost:8888 ](http://localhost:8888)