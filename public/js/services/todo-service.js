angular.module('todoService', [])

	.factory('Todos', ['$http',function($http) {
		return {
			get : function() {
				return $http.get('/api/v1/todos');
			},
			create : function(todoData) {
				return $http.post('/api/v1/todos', todoData);
			},
			update : function(todoData) {
				return $http.put('/api/v1/todos/' + todoData.id, todoData);
			},
			delete : function(id) {
				return $http.delete('/api/v1/todos/' + id);
			}
		}
	}]);