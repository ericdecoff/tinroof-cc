
module.exports = function (app) {

    const pg = require('pg');
    
    const connectionString = process.env.DATABASE_URL || 'postgres://admin@localhost:5432/todos';


    app.get('/api/v1/todos', function (req, res) {
        console.log('(get)/api/v1/todos');

        const results = [];

        pg.connect(connectionString, (err, client, done) => {
            // Error Handler
            if(err) {
                done();
                console.log(err);
                return res.status(500).json({success: false, data: err});
            }

            // SQL Query > Select Data
            const query = client.query('SELECT * FROM items ORDER BY id ASC;');
            
            // Stream results back one row at a time
            query.on('row', (row) => {
                results.push(row);
            });

            // After all data is returned, close connection and return results
            query.on('end', () => {
                done();
                return res.json(results);
            });
        });
  
    });

    app.post('/api/v1/todos', function (req, res) {
        console.log('(post)/api/v1/todos')
        
        const results = [];

        const data = {text: req.body.text, complete: false};

        pg.connect(connectionString, (err, client, done) => {
            // Error Handler
            if(err) {
                done();
                console.log(err);
                return res.status(500).json({success: false, data: err});
            }

            // SQL Query > Insert Data
            client.query('INSERT INTO items(text, complete) values($1, $2)',
                [data.text, data.complete]
            );
            
            // SQL Query > Select Data
            const query = client.query('SELECT * FROM items ORDER BY id ASC');
            
            // Stream results back one row at a time
            query.on('row', (row) => {
                results.push(row);
            });

            // After all data is returned, close connection and return results
            query.on('end', () => {
                done();
                return res.json(results);
            });
        });

    });

    app.put('/api/v1/todos/:todo_id', function (req, res) {
        console.log('(put)/api/v1/todos/:todo_id');

        const results = [];

        const id = req.params.todo_id;

        const data = {text: req.body.text, complete: req.body.complete};
 
         pg.connect(connectionString, (err, client, done) => {
            // Error Handler
            if(err) {
                done();
                console.log(err);
                return res.status(500).json({success: false, data: err});
            }
            
            // SQL Query > Update Data
            client.query('UPDATE items SET text=($1), complete=($2) WHERE id=($3)',
                [data.text, data.complete, id]
            );

            // SQL Query > Select Data
            const query = client.query("SELECT * FROM items ORDER BY id ASC");
    
            // Stream results back one row at a time
            query.on('row', (row) => {
                results.push(row);
            });
    
            // After all data is returned, close connection and return results
            query.on('end', function() {
                done();
                return res.json(results);
            });
        });        
    });


    app.delete('/api/v1/todos/:todo_id', function (req, res) {
        console.log('(delete)/api/v1/todos/:todo_id');
        console.log(req.params);

        const results = [];
        
        const id = req.params.todo_id;
  
        pg.connect(connectionString, (err, client, done) => {
            // Error Handler
            if(err) {
                done();
                console.log(err);
                return res.status(500).json({success: false, data: err});
            }

            // SQL Query > Delete Data
            client.query('DELETE FROM items WHERE id=($1)', [id]);
    
            // SQL Query > Select Data
            var query = client.query('SELECT * FROM items ORDER BY id ASC');
    
            // Stream results back one row at a time
            query.on('row', (row) => {
                results.push(row);
            });

            // After all data is returned, close connection and return results
            query.on('end', () => {
                done();
                return res.json(results);
            });
        });        
    });

    app.get('*', function (req, res) {
        res.sendFile(__dirname + '/public/index.html');
    });
};